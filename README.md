# fosclass kenalan dengan React JS dan Firebase

#Membangun WebApps belanja online sederhana dan di host ke firebase

#TODOS :

- [ ] Setup Development
        - Install Nodejs
            `sudo apt install nodejs`
        - Install NPM
            `sudo apt install npm`
- [ ] create react app
        - `npx create-react-app my-app`
        - ATAU :
        - `npm init react-app my-app`

- [ ] Setup Firebase
- [ ] Pengenalan Arsitektur (API, SERVERLESS)
- [ ] Intro To React
- [ ] Case => Toko Online
- [ ] Case => Shopping Cart
- [ ] Deploy Ke firebase dengan domain